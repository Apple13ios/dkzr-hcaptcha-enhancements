<?php
/**
 * Plugin Name: dkzr hCaptcha enhancements
 * Plugin URI: https://dkzr.nl
 * Update URI: https://dkzr.dev/wp/plugins/dkzr-hcaptcha-enhancements/
 * Description: Allow for global hCaptcha settings using constants in wp-config. Define constants `dkzr_hcaptcha_api_key`, `dkzr_hcaptcha_secret_key`, `dkzr_hcaptcha_theme` and `dkzr_hcaptcha_size`.
 * Author: Joost de Keijzer
 * Version: 2.0
 */

class dkzrHcaptchaEnhancements {
  public function __construct() {
    add_action( 'plugin_loaded', [ $this, 'init' ] );
    add_action( 'wp_head', [ $this, 'styles' ] );
    add_action( 'login_head', [ $this, 'styles' ] );
    add_action( 'admin_head', [ $this, 'styles' ] );
    add_action( 'settings_page_hcaptcha-options', [ $this, 'settings_page_after' ], 99999 );

    // prevent delay in widget display, helps when using Safari & Keychain
    add_filter( 'hcap_delay_api', '__return_zero' );
  }

  public function init() {
    if ( function_exists( 'hcap_options' ) ) {
      remove_action( 'plugin_loaded', [ $this, 'init' ] );
      $this->setup();
    }
  }

  public function styles() {
    $svg_url = esc_attr( plugins_url( 'dashicon-rotate.svg', __FILE__ ) );
    echo <<<EOS
<style type="text/css">
  div.h-captcha {
    min-height: 40px;
    background-image: url($svg_url);
    background-repeat: no-repeat;
    background-position: center;
  }
</style>
EOS;
  }

  public function settings_page_after() {
    wp_enqueue_script( 'jquery' );
    $disable = [];
    foreach( hcap_options() as $name => $settings ) {
      $constant = strtoupper( str_replace( '-', '_', $name ) );
      if ( defined( $constant ) ) {
        $disable[] = "[name={$name}]";
      }
    }
    $disable = implode( ',', $disable );
    echo <<<EOJ
<script type="text/javascript">
  jQuery( 'input[type=password]' ).attr( { autocomplete: 'off', spellcheck: false } ).parent( 'form' ).attr( 'autocomplete', 'off' );
  jQuery( '$disable' ).attr( { disabled: 'disabled', readonly: 'readonly' } );
  jQuery( 'input[type=text], input[type=checkbox], input[type=password], select' ).attr( 'title', function() {
    return 'Constant name: "' + this.name.replace('-', '_').toUpperCase() + '"';
  } );
</script>
EOJ;

    printf(
      '<div class="notice notice-info"><p>%s<br>%s</p></div>',
      esc_html__( 'The disabled settings are configured using constants and the dkzr hCaptcha enhancement plugin', 'dkzr-hcaptcha-enhancements' ),
      esc_html__( 'Hover the element to see the constant name to use in wp-config.', 'dkzr-hcaptcha-enhancements' )
    );
  }

  protected function setup() {
    foreach( hcap_options() as $name => $settings ) {
      $constant = strtoupper( str_replace( '-', '_', $name ) );
      if ( defined( $constant ) ) {
        add_filter( "pre_option_$name", function ( $value, $option, $default ) use ( $constant, $name, $settings ) {
          return constant( $constant );
        }, 10, 3 );
      }
    }
  }
}
$dkzrHcaptchaEnhancements = new dkzrHcaptchaEnhancements();
